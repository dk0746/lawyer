/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$('.custom-error').hide();
function validateString(value) {
    var regexp = /^[a-zA-Z]*$/;
    return ((value !== undefined) && value.length > 0 && value !== null) ? regexp.test(value) : true;
}
function validateMobile(value) {
    var regexp = /[7-9]{1}[0-9]{9}$/;
    return ((value !== undefined) && value.length > 0 && value !== null) ? regexp.test(value) : true;
}
function validateCity(value) {
    var regexp = /^([a-zA-Z\u0080-\u024F]+(?:.|. |-| |'))*[a-zA-Z\u0080-\u024F]{1,28}$/;
    return ((value !== undefined) && value.length > 0 && value !== null) ? regexp.test(value) : true;
}
function maxlength(value, maxlength) {
    return ((value !== undefined) && value !== null) ? value.length <= maxlength : true;
}
function minlength(value, minlength) {
    return ((value !== undefined) && value !== null) ? value.length >= minlength : true;
}

function validateDegree(value) {
    var regexp = /^[a-zA-Z0-9_.-]*$/;
    return ((value !== undefined) && value.length > 0 && value !== null) ? regexp.test(value) : true;
}
function reserError(id) {
    if (id !== undefined) {
        $('#' + id).hide();
    }
}
function validateRegisterForm() {
    $('.custom-error').hide();
    var response = {
        status: 1,
        message: ''
    }
    var regexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if ($('#registerFirstName').val() === undefined || $('#registerFirstName').val() == '') {
        $('#error_registerFirstName').show().text('This field is required.');
        response.status = 0;
    } else {
        if (!validateString($('#registerFirstName').val())) {
            $('#error_registerFirstName').show().text('Please enter valid first name.');
            response.status = 0;
        }
        if (!minlength($('#registerFirstName').val(), 3)) {
            $('#error_registerFirstName').show().text('Minimum 3 character required.');
            response.status = 0;
        }
        if (!maxlength($('#registerFirstName').val(), 50)) {
            $('#error_registerFirstName').show().text('Maximum 50 character alloweded.');
            response.status = 0;
        }
    }
    if (($('#registerLastName').val() === undefined || $('#registerLastName').val() == '')) {
        $('#error_registerLastName').show().text('This field is required.');
        response.status = 0;
    } else {
        if (!validateString($('#registerLastName').val())) {
            $('#error_registerLastName').show().text('Please enter valid last name.');
            response.status = 0;
        }
        if (!minlength($('#registerLastName').val(), 3)) {
            $('#error_registerLastName').show().text('Minimum 3 character required.');
            response.status = 0;
        }
        if (!maxlength($('#registerLastName').val(), 50)) {
            $('#error_registerLastName').show().text('Maximum 50 character alloweded.');
            response.status = 0;
        }
    }
    if (($('#registerInputEmail').val() === undefined || $('#registerInputEmail').val() == '')) {
        $('#error_registerInputEmail').show().text('This field is required.');
        response.status = 0;
    }
    if (($('#registerInputEmail').val() && !regexp.test($('#registerInputEmail').val()))) {
        $('#error_registerInputEmail').show().text('Please enter valid email.');
        response.status = 0;
    }

    if (($('#registerInputMobile').val() === undefined || $('#registerInputMobile').val() == '')) {
        $('#error_registerInputMobile').show().text('This field is required.');
        response.status = 0;
    } else {
        if (!validateMobile($('#registerInputMobile').val())) {
            $('#error_registerInputMobile').show().text('Please enter valid mobile number.');
            response.status = 0;
        }
    }

    if ($('#registerGender').val() === undefined || $('#registerGender').val() == '') {
        $('#error_registerGender').show().text('This field is required.');
        response.status = 0;
    }
//    if ($('#registerSelectType').val() === undefined || $('#registerSelectType').val() == '') {
//        $('#error_registerSelectType').show().text('This field is required.');
//        response.status = 0;
//    }
    if (($('#registerDist').val() === undefined || $('#registerDist').val() == '')) {
        $('#error_registerDist').show().text('This field is required.');

        response.status = 0;
    } else {
        if (!validateCity($('#registerDist').val())) {
            $('#error_registerDist').show().text('Please enter valid district name.');
            response.status = 0;
        }
        if (!minlength($('#registerDist').val(), 3)) {
            $('#error_registerDist').show().text('Minimum 3 character required.');
            response.status = 0;
        }
        if (!maxlength($('#registerDist').val(), 50)) {
            $('#error_registerDist').show().text('Maximum 50 character alloweded.');
            response.status = 0;
        }
    }
    if (($('#registerCity').val() === undefined || $('#registerCity').val() == '')) {
        $('#error_registerCity').show().text('This field is required.');
        response.status = 0;
    } else {
        if (!validateCity($('#registerCity').val())) {
            $('#error_registerCity').show().text('Please enter valid district name.');
            response.status = 0;
        }
        if (!minlength($('#registerCity').val(), 3)) {
            $('#error_registerCity').show().text('Minimum 3 character required.');
            response.status = 0;
        }
        if (!maxlength($('#registerCity').val(), 50)) {
            $('#error_registerCity').show().text('Maximum 50 character alloweded.');
            response.status = 0;
        }
    }

    if (($('#registerInputPassword').val() === undefined || $('#registerInputPassword').val() == '')) {
        $('#error_registerInputPassword').show().text('This field is required.');
        response.status = 0;
    }
    if (($('#registerRepeatPassword').val() === undefined || $('#registerRepeatPassword').val() == '')) {
        $('#error_registerRepeatPassword').show().text('This field is required.');
        response.status = 0;
    }
    if (($('#registerInputPassword').val() && $('#registerRepeatPassword').val()) && $('#registerInputPassword').val() !== $('#registerRepeatPassword').val()) {
        $('#error_registerRepeatPassword').show().text('Password and confirm password should be same.');
        response.status = 0;
    }

    return response;
}



function validateChangePasswordForm() {
    $('.custom-error').hide();
    var response = {
        status: 1,
        message: ''
    }

    if (($('#oldPassword').val() === undefined || $('#oldPassword').val() == '')) {
        $('#error_oldPassword').show().text('This field is required.');
        response.status = 0;
    } else {
        if (!minlength($('#oldPassword').val(), 6)) {
            $('#error_oldPassword').show().text('Old password length should be greater than 5.');
            response.status = 0;
        }
    }
    if (($('#password').val() === undefined || $('#password').val() == '')) {
        $('#error_password').show().text('This field is required.');
        response.status = 0;
    } else {
        if (!minlength($('#password').val(), 6)) {
            $('#error_password').show().text('Old password length should be greater than 5.');
            response.status = 0;
        }
    }
    if (($('#confirmPassword').val() === undefined || $('#confirmPassword').val() == '')) {
        $('#error_confirmPassword').show().text('This field is required.');
        response.status = 0;
    }

    if (($('#password').val() && $('#oldPassword').val()) && $('#password').val() == $('#oldPassword').val()) {
        $('#error_password').show().text('Old password and new password should not be same.');
        response.status = 0;
    }
    if (($('#password').val() && $('#confirmPassword').val()) && $('#password').val() !== $('#confirmPassword').val()) {
        $('#error_confirmPassword').show().text('Password and confirm password should be same.');
        response.status = 0;
    }
    return response;
}



function validateProfileForm() {
    $('.custom-error').hide();
    var response = {
        status: 1,
        message: ''
    }
    var regexp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if ($('#registerFirstName').val() === undefined || $('#registerFirstName').val() == '') {
        $('#error_registerFirstName').show().text('This field is required.');
        response.status = 0;
    } else {
        if (!validateString($('#registerFirstName').val())) {
            $('#error_registerFirstName').show().text('Please enter valid first name.');
            response.status = 0;
        }
        if (!minlength($('#registerFirstName').val(), 3)) {
            $('#error_registerFirstName').show().text('Minimum 3 character required.');
            response.status = 0;
        }
        if (!maxlength($('#registerFirstName').val(), 50)) {
            $('#error_registerFirstName').show().text('Maximum 50 character alloweded.');
            response.status = 0;
        }
    }
    if (($('#registerLastName').val() === undefined || $('#registerLastName').val() == '')) {
        $('#error_registerLastName').show().text('This field is required.');
        response.status = 0;
    } else {
        if (!validateString($('#registerLastName').val())) {
            $('#error_registerLastName').show().text('Please enter valid last name.');
            response.status = 0;
        }
        if (!minlength($('#registerLastName').val(), 3)) {
            $('#error_registerLastName').show().text('Minimum 3 character required.');
            response.status = 0;
        }
        if (!maxlength($('#registerLastName').val(), 50)) {
            $('#error_registerLastName').show().text('Maximum 50 character alloweded.');
            response.status = 0;
        }
    }


    if ($('#registerGender').val() === undefined || $('#registerGender').val() == '') {
        $('#error_registerGender').show().text('This field is required.');
        response.status = 0;
    }

    if (($('#registerDist').val() === undefined || $('#registerDist').val() == '')) {
        $('#error_registerDist').show().text('This field is required.');

        response.status = 0;
    } else {
        if (!validateCity($('#registerDist').val())) {
            $('#error_registerDist').show().text('Please enter valid district name.');
            response.status = 0;
        }
        if (!minlength($('#registerDist').val(), 3)) {
            $('#error_registerDist').show().text('Minimum 3 character required.');
            response.status = 0;
        }
        if (!maxlength($('#registerDist').val(), 50)) {
            $('#error_registerDist').show().text('Maximum 50 character alloweded.');
            response.status = 0;
        }
    }

    if (($('#registerCity').val() === undefined || $('#registerCity').val() == '')) {
        $('#error_registerCity').show().text('This field is required.');
        response.status = 0;
    } else {
        if (!validateCity($('#registerCity').val())) {
            $('#error_registerCity').show().text('Please enter valid district name.');
            response.status = 0;
        }
        if (!minlength($('#registerCity').val(), 3)) {
            $('#error_registerCity').show().text('Minimum 3 character required.');
            response.status = 0;
        }
        if (!maxlength($('#registerCity').val(), 50)) {
            $('#error_registerCity').show().text('Maximum 50 character alloweded.');
            response.status = 0;
        }
    }

    if ($('#h_degree').val() !== undefined && ('#h_degree') != '') {
        if (!minlength($('#h_degree').val(), 2)) {
            $('#error_h_degree').show().text('Minimum 2 character required.');
            response.status = 0;
        }
        if (!maxlength($('#h_degree').val(), 50)) {
            $('#error_h_degree').show().text('Maximum 50 character alloweded.');
            response.status = 0;
        }
        if (!validateDegree($('#specialization').val())) {
            $('#error_h_degree').show().text('Please enter valid specialization.');
            response.status = 0;
        }
        if (!validateDegree($('#h_degree').val())) {
            $('#error_h_degree').show().text('Please enter valid degree.');
            response.status = 0;
        }
    }

    if ($('#specialization').val() !== undefined && ('#h_degree') != '') {
        if (!minlength($('#specialization').val(), 3)) {
            $('#error_specialization').show().text('Minimum 3 character required.');
            response.status = 0;
        }
        if (!maxlength($('#specialization').val(), 50)) {
            $('#error_specialization').show().text('Maximum 50 character alloweded.');
            response.status = 0;
        }
        if (!validateDegree($('#specialization').val())) {
            $('#error_specialization').show().text('Please enter valid specialization.');
            response.status = 0;
        }
    }

    return response;
}

$(function () {
    $("#registerForm").on('submit', function (e) {

        e.preventDefault();
        showLoader()
        var registerForm = $(this);
        var data = registerForm.serialize();
        var validateResponse = validateRegisterForm(data)
        if (validateResponse.status == 0) {
//            maketoast(validateResponse.message, 'Error', 'danger')
            hideLoader();
            return false;
        }
        
        $.ajax({
            url: registerForm.attr('action'),
            type: 'post',
            dataType: 'json',
            data: data,
            success: function (response) {

                if (response.status && response.status == 1 && response.data !== undefined && response.data.requestId) {
                    var url = $('#base_url').val() + 'lawyer/verifyOtp/' + response.data['requestId'];
                    $(location).attr('href', url);
                    setTimeout(function () {
                        $('#successMssg').hide();
                    }, 5000);
                    hideLoader();
                    return true;
                } else {
                    if (response.message) {
                        maketoast(response.message, 'Error', 'danger');
                        hideLoader();
                        return false;
                    }
                    hideLoader();
                    maketoast('Something went wrong!!', 'Error', 'danger');
                    return false;
                }

            }
        });
    });

    $("#changePasswordForm").on('submit', function (e) {

        e.preventDefault();
        showLoader();
        var changeForm = $(this);
        var data = changeForm.serialize();
        var validateResponse = validateChangePasswordForm(data)
        if (validateResponse.status == 0) {
//            maketoast(validateResponse.message, 'Error', 'danger')
            hideLoader();
            return false;
        }
        $.ajax({
            url: changeForm.attr('action'),
            type: 'post',
            dataType: 'json',
            data: data,
            success: function (response) {
                if (response.status && response.status == 1) {
                    $('#successMssg').show();
                    $('#successMssg').text(response.message);
                    hideLoader();
                    setTimeout(function () {
                        $('#successMssg').hide();
                    }, 5000);

//                    maketoast(response.message, 'Success', 'Success');
                    return true;
//                    var url = $('#base_url').val() + 'site/login';
//                    $(location).attr('href', url);
                } else {
                    if (response.message) {
                        hideLoader();
                        maketoast(response.message, 'Error', 'danger');
                        return false;
                    }
                    hideLoader();
                    maketoast('Something went wrong!!', 'Error', 'danger');
                    return false;
                }

            }
        });
    });


    $("#userUpdate").on('submit', function (e) {

        e.preventDefault();
        showLoader();
        var changeForm = $(this);

        var data = changeForm.serialize();
        var validateResponse = validateProfileForm(data)
        if (validateResponse.status == 0) {
            hideLoader();
//            maketoast(validateResponse.message, 'Error', 'danger')
            return false;
        }
        $.ajax({
            type: 'POST',
            url: changeForm.attr('action'),
            type: 'post',
            dataType: 'json',
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function (response) {
                if (response.status && response.status == 1) {
                    $('#successMssg').show();
                    $('#successMssg').text(response.message);
                    hideLoader();
                    setTimeout(function () {
                        $('#successMssg').hide();
                    }, 5000);

//                    maketoast(response.message, 'Success', 'Success');
                    return true;
//                    var url = $('#base_url').val() + 'site/login';
//                    $(location).attr('href', url);
                } else {
                    if (response.message) {
                        hideLoader();
                        maketoast(response.message, 'Error', 'danger');
                        return false;
                    }
                    hideLoader();
                    maketoast('Something went wrong!!', 'Error', 'danger');
                    return false;
                }

            },
            error: function (){
                hideLoader();
            }
        });
    });
});

var interval;
var codetmpl = "<code>%codeobj%</code><br><code>%codestr%</code>";





function maketoast(message, title, toastPriority)
{
//    evt.preventDefault();

    var options =
            {
                priority: toastPriority,
                title: title,
                message: message
            };

    if (options.priority === '<use default>')
    {
        options.priority = null;
    }

    var codeobj = [];
    var codestr = [];

    var labels = ['message', 'title', 'priority'];
    for (var i = 0, l = labels.length; i < l; i += 1)
    {
        if (options[labels[i]] !== null)
        {
            codeobj.push([labels[i], "'" + options[labels[i]] + "'"].join(' : '));
        }

        codestr.push((options[labels[i]] !== null) ? "'" + options[labels[i]] + "'" : 'null');
    }

    if (codestr[2] === 'null')
    {
        codestr.pop();
        if (codestr[1] === 'null')
        {
            codestr.pop();
        }
    }

    codeobj = "$.toaster({ " + codeobj.join(", ") + " });"
    codestr = "$.toaster(" + codestr.join(", ") + ");"
    $.toaster(options);
}   
