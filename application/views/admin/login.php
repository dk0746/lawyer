<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('common/header');
?>

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
                                    <?php
                                    if (!empty($this->session->flashdata('msg_success'))) {
                                        ?>
                                        <div class="alert alert-success">

                                            <?php echo $this->session->flashdata('msg_success'); ?>
                                        </div>
                                        <?php
                                    }
                                    if ($this->session->flashdata('msg_error')) {
                                        ?>
                                        <div class="alert alert-danger">
                                            <?php echo $this->session->flashdata('msg_error'); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <form class="user" method="POST" action="<?php echo base_url(); ?>admin/login">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-user" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter Email Address..." required="true">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-user" name="password" id="password" placeholder="Password" required="true">
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <input type="checkbox" class="custom-control-input" id="customCheck">
                                            <label class="custom-control-label" for="customCheck">Remember Me</label>
                                        </div>
                                    </div>
                                    <input type="submit" value="Login" class="btn btn-primary btn-user btn-block">

                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="<?php echo base_url(); ?>admin/forgot">Forgot Password?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

<?php
$this->load->view('common/footer');
?>