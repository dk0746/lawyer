<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?> 
<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <?php
            $role = $this->session->userdata('role');
            if ($role == 'admin') {
                ?>

                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url(); ?>admin/home">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <!--<img src="<?php echo base_url(); ?>assets/images/ordrordr.svg" alt="Girl in a jacket">-->
                    </div>
                    <div class="sidebar-brand-text mx-3">OrdrOrdr.com </div>
                </a>
            <?php } else {
                ?>
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url(); ?>lawyer/home">
                    <div class="sidebar-brand-icon rotate-n-15">
                        <!--<img src="<?php echo base_url(); ?>assets/images/ordrordr.svg" alt="Girl in a jacket">-->
                    </div>
                    <div class="sidebar-brand-text mx-3">OrdrOrdr.com </div>
                </a>
            <?php }
            ?>
            <!-- Divider -->
            <hr class="sidebar-divider my-0">
            <?php
            if ($role == 'admin') {
                ?>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo base_url(); ?>admin/home">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></a>
                </li>
            <?php } else {
                ?>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo base_url(); ?>lawyer/home">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></a>
                </li>
            <?php }
            ?>


            <!-- Nav Item - Dashboard -->


            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Addons
            </div>



            <!-- Nav Item - Charts -->
            <?php
            if ($role == 'admin') {
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url(); ?>admin/lawyer">
                        <i class="fas fa-fw fa-chart-area"></i>
                        <span>Lawyers</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url(); ?>admin/users">
                        <i class="fas fa-fw fa-chart-area"></i>
                        <span>Users</span></a>
                </li>
            <?php } else {
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url(); ?>lawyer/appointment">
                        <i class="fas fa-fw fa-chart-area"></i>
                        <span>Appointment</span></a>
                </li>
                <?php
            }
            ?>
        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
                <?php
                $this->load->view('admin/layout/nav_bar');
                ?>
