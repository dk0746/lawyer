<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('admin/layout/header');
$this->load->view('admin/layout/side_bar');
?>
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?php echo $title; ?></h1>
    <div class="alert alert-success" id="successMssg" style="display: none;">
    </div>
    <div class="alert alert-danger" id="errorMssg" style="display: none;">
    </div>

    <hr/>
    <form class="user" id="changePasswordForm" name="changePasswordForm" action="<?php echo base_url(); ?>admin/changePassword" method="POST" enctype='application/json'>
        <div class="row">

            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>Old Password:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <input type="password" class="form-control form-control-user" name="oldPassword" id="oldPassword" placeholder="Old Password" value="" onkeydown="reserError('error_oldPassword')">
                    <label class="custom-error" id="error_oldPassword"></label>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>New Password:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <input type="password" class="form-control form-control-user" name="password" id="password" placeholder="New Password" value="" onkeydown="reserError('error_password')">
                    <label class="custom-error" id="error_password"></label>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>Confirm Password:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <input type="password" class="form-control form-control-user" name="confirmPassword" id="confirmPassword" placeholder="Confirm Password" value="" onkeydown="reserError('error_confirmPassword')">
                    <label class="custom-error" id="error_confirmPassword"></label>
                </div>
                <div class="clearer"></div>
            </div>

            <div class="col-lg-12" style="padding-top: 30px;">
                <div class="col-sm-6 mg-3 mb-sm-0" style="">
                    <!--<input type="submit" class="btn btn-primary btn-user btn-block" value="Submit">-->
                </div>
                <div class="col-sm-6 mg-3 mb-sm-0" style="">
                    <input type="submit" class="btn btn-primary btn-user" style="float: right" value="Submit">
                </div>
                <div class="col-sm-3 mg-3 mb-sm-0" style="">
                    <a href="<?php echo base_url(); ?>lawyer/home" class="btn btn-info btn-user" value="Submit">Cancel</a>
                </div>

                <div class="clearer"></div>
            </div>

        </div>
    </form>

</div>
<?php
$this->load->view('admin/layout/footer');
?>
<script src="<?php echo base_url(); ?>assets/js/custom/jquery.toaster.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom/loyer.js"></script>
