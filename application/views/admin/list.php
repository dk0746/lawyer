<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('admin/layout/header');
$this->load->view('admin/layout/side_bar');
?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?php echo $title; ?></h1>
    <div class="alert alert-success" id="successMssg" style="display: none;">
    </div>
    <div class="alert alert-danger" id="errorMssg" style="display: none;">
    </div>
    <table id='empTable' class='display dataTable'>

        <thead>
            <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Mobile</th>
                <th>Email</th>
                <th>Dist</th>
                <th>City</th>
                <th>Action</th>
            </tr>
        </thead>

    </table>
</div>
</div>
<?php
$this->load->view('admin/layout/footer');
?>
<!-- Script -->
<script type="text/javascript">

    function viewLoyer(loyer) {
        var url = '<?php echo base_url() ?>admin/lawyer/' + loyer;
        $(location).attr('href', url);
    }
    function BlockLoyer(loyer) {

        var url = '<?php echo base_url() ?>admin/lawyer/process';

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {loyer: loyer, status: 'block'},
            success: function (response) {
                if (response.status && response.status == 1) {
                    $('#successMssg').show();
                    $('#successMssg').text('Loyer blocked successfully.');
                    setTimeout(function () {
                        $('#successMssg').hide();
                        location.reload(true);
                    }, 5000);

                } else {
                    if (response.message) {
                        $('#errorMssg').show();
                        $('#errorMssg').text('Something went wrong!!');
                        setTimeout(function () {
                            $('#errorMssg').hide();
                        }, 5000);
                        return false;
                    }
                    alert('Something went wrong!!');
                    return false;
                }

            }
        });
    }
    $(document).ready(function () {
        $('#empTable').DataTable({
            'processing': true,
            'serverSide': true,
            'serverMethod': 'post',
            'ajax': {
                'url': '<?php echo base_url() ?>admin/active'
            },
            'columns': [
                {data: 0},
                {data: 1},
                {data: 2},
                {data: 3},
                {data: 4},
                {data: 5},
                {data: 8},
            ]
        });


    });
</script>