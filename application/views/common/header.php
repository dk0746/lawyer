<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?php echo $title; ?></title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url(); ?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="<?php echo base_url(); ?>assets/css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/jquery.mobile-1.3.2.min.css" rel="stylesheet">

    </head>

    <body class="bg-gradient-primary">

        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
        <div id='ajax-loader' class="modal fade in">
            <div class='ajax-loader' >
                <span >
                    <div class="spinner-loader">
                        <div id="circularG">
                            <span>Loading…</span>
                            <div id="circularG_1" class="circularG"></div>
                            <div id="circularG_2" class="circularG"></div>
                            <div id="circularG_3" class="circularG"></div>
                            <div id="circularG_4" class="circularG"></div>
                            <div id="circularG_5" class="circularG"></div>
                            <div id="circularG_6" class="circularG"></div>
                            <div id="circularG_7" class="circularG"></div>
                            <div id="circularG_8" class="circularG"></div>
                        </div>
                    </div>
                </span>
            </div>
        </div>
        <div id='ajax-loader-note' class="modal fade in">
            <div class='ajax-loader'>
                <div class='span-text' style="margin-top:15%!important">
                    <div class="spinner-loader">
                        <div id="circularG">
                            <span class='loading-text'>Loading…</span>
                            <div id="circularG_1" class="circularG"></div>
                            <div id="circularG_2" class="circularG"></div>
                            <div id="circularG_3" class="circularG"></div>
                            <div id="circularG_4" class="circularG"></div>
                            <div id="circularG_5" class="circularG"></div>
                            <div id="circularG_6" class="circularG"></div>
                            <div id="circularG_7" class="circularG"></div>
                            <div id="circularG_8" class="circularG"></div>
                        </div>
                    </div>
                </div>
                <div class="span-text display-text">....</div>
            </div>
        </div>
        <script>
            var loaderState = "hidden"; // "hidden", "visible", "fading"
            var requestedLoaderState = "hidden"; // "hidden", "visible"
            var loaderCount = 0;

            function loaderFadeDone() {
                loaderState = "hidden";
            }
            function showLoader() {
                loaderCount++;
                requestedLoaderState = "visible";
                if ("fading" != loaderState) {
                    loaderState = "s";
                }
                $('#ajax-loader').show();
            }
            function hideLoader() {
                loaderCount--;
                requestedLoaderState = "hidden";
                if ("visible" == loaderState) {
                    loaderState = "fading";
                }
                if (loaderCount <= 0) {
                    $('#ajax-loader').show().fadeOut(400 /* default jquery value */, "swing" /* default jquery value */, loaderFadeDone);
                }

            }
           
        </script>