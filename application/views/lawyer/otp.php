<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('common/header');
?>
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <div class="alert alert-success" id="successMssg" style="display: none;">
                                    </div>
                                    <div class="alert alert-danger" id="errorMssg" style="display: none;">
                                    </div>
                                    <?php
                                    if (!empty($this->session->flashdata('msg_success'))) {
                                        ?>
                                        <div class="alert alert-success">

                                            <?php echo $this->session->flashdata('msg_success'); ?>
                                        </div>
                                        <?php
                                    }
                                    if ($this->session->flashdata('msg_error')) {
                                        ?>
                                        <div class="alert alert-danger">
                                            <?php echo $this->session->flashdata('msg_error'); ?>
                                        </div>
                                    <?php } ?>
                                    <h1 class="h4 text-gray-900 mb-2">Verify OTP</h1>
                                    <p class="mb-4">We have sent you an OTP on</p>
                                    <p class="mb-4">+919637350746</p>
                                </div>
                                <form class="user" id="user_otp" method="POST" action="<?php echo base_url(); ?>lawyer/verifyOtp/<?php echo $requestId; ?>">
                                    <input type="hidden" name="requestId" id="requestId" value="<?php echo $requestId ?>">
                                    <div class="form-group">
                                        <input type="password" name='otp' class="form-control form-control-user" id="otpInput" aria-describedby="emailHelp" placeholder="Enter OTP..." maxlength="6">
                                        <label class="custom-error" id="error_otpInput"></label>
                                        <div>OTP will valid for <span id="timer"></span></div>
                                    </div>
                                    <input type= "hidden" name="expired" value="0">
                                    <input type="submit" name="submit" value="Submit" class="btn btn-primary btn-user btn-block">
                                    <input type="button" value="Resend" id="btn-resend" class="btn btn-primary btn-user btn-block" onclick="resend()">
                                </form>
                                <hr>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<?php
$this->load->view('common/footer');
?>
<script>

    $(function () {
        function maxlength(value, maxlength) {
            return ((value !== undefined) && value !== null) ? value.length <= maxlength : true;
        }
        function minlength(value, minlength) {
            return ((value !== undefined) && value !== null) ? value.length >= minlength : true;
        }

        $("#user_otp").on('submit', function (e) {

            e.preventDefault();
            $('.custom-error').hide();
            var otpForm = $(this);
            var data = otpForm.serialize();

            if ($('#otpInput').val() === undefined || $('#otpInput').val() == '') {
                $('#error_otpInput').show().text('Please enter OTP.');
                return false;
            } else {
                if (!minlength($('#otpInput').val(), 6)) {
                    $('#error_otpInput').show().text('Please enter valid OTP.');
                    return false;
                }
            }

            $.ajax({
                url: otpForm.attr('action'),
                type: 'post',
                dataType: 'json',
                data: data,
                success: function (response) {

                    if (response.status && response.status == 1) {
                        var url = $('#base_url').val() + 'lawyer/success/';
                        $(location).attr('href', url);
                        return true;
                    } else {
                        if (response.message) {
                            $('#errorMssg').show();
                            $('#errorMssg').text(response.message);
                            return false;
                        }
                        maketoast('Something went wrong!!', 'Error', 'danger');
                        return false;
                    }

                }
            });
        });

    });
    function resend() {
        var requestId = $('#requestId').val();
        var url = '<?php echo base_url() ?>lawyer/resendOtp/' + requestId;

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {requestId: requestId},
            success: function (response) {
                if (response.status) {
                    if (response.status == 1) {
                        $('#errorMssg').hide();
                        $('#successMssg').show();
                        $('#successMssg').text(response.message);
                        setTimeout(function () {
                            $('#successMssg').hide();
                            location.reload(true);
                        }, 5000);
                        $('#expired').val(0);
                        timer(120);
                    } else {
                        $('#errorMssg').show();
                        $('#errorMssg').text(response.message);
                        setTimeout(function () {
                            $('#errorMssg').hide();
                        }, 5000);
                        $('#btn-resend').hide();
                        return false;
                    }


                } else {
                    if (response.message) {
                        $('#errorMssg').show();
                        $('#errorMssg').text(response.message);
                        setTimeout(function () {
                            $('#errorMssg').hide();
                        }, 5000);
                        return false;
                    }
                    alert('Something went wrong!!');
                    return false;
                }

            }
        });
    }

    let timerOn = true;

    function timer(remaining) {
        if ($('#expired').val() == 1) {
            return;
        }
        var m = Math.floor(remaining / 60);
        var s = remaining % 60;

        m = m < 10 ? '0' + m : m;
        s = s < 10 ? '0' + s : s;
        document.getElementById('timer').innerHTML = m + ':' + s;
        remaining -= 1;

        if (remaining >= 0 && timerOn) {
            setTimeout(function () {
                timer(remaining);
            }, 1000);
            return;
        }

        if (!timerOn) {
            // Do validate stuff here
            return;
        }

        // Do timeout stuff here
        expired();
//        alert('Timeout for otp');
        $('#expired').val(1);
    }

    function expired() {
        var requestId = $('#requestId').val();
        var url = '<?php echo base_url() ?>lawyer/expired';

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {requestId: requestId},
            success: function (response) {
                if (response.status && response.status == 1) {
                    $('#errorMssg').show();
                    $('#errorMssg').text(response.message);
                    setTimeout(function () {
                        $('#successMssg').hide();
                    }, 5000);
//                    timer(0);
                    return false;

                } else {
                    if (response.message) {
                        $('#errorMssg').show();
                        $('#errorMssg').text(response.message);
                        setTimeout(function () {
                            $('#errorMssg').hide();
                        }, 5000);
                        $('#expired').val(1);
                        return false;
                    }
                    alert('Something went wrong!!');
                    return false;
                }

            }
        });
    }

    timer(120);
</script>