<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('common/header');
?>


<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
                <div class="col-lg-7">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                            <div class="alert alert-success" id="successMssg" style="display: none;">
                            </div>
                            <div class="alert alert-danger" id="errorMssg" style="display: none;">
                            </div>
                        </div>
                        <input type="hidden" value="<?php echo base_url(); ?>" id="base_url" name="base_url">
                        <form class="user" id="registerForm" name="registerForm" action="<?php echo base_url(); ?>add" method="POST" enctype='application/json'>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input type="text" class="form-control form-control-user" name="firstName" id="registerFirstName" placeholder="First Name" onkeydown="reserError('error_registerFirstName')">
                                    <label class="custom-error" id="error_registerFirstName"></label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="lastName" id="registerLastName" placeholder="Last Name" onkeydown="reserError('error_registerLastName')">
                                    <label class="custom-error" id="error_registerLastName"></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control form-control-user" name="email" id="registerInputEmail" placeholder="Email Address" onkeydown="reserError('error_registerInputEmail')">
                                <label class="custom-error" id="error_registerInputEmail"></label>
                            </div>
                            <div class="form-group">
                                <input type=text"" class="form-control form-control-user" name="mobile" id="registerInputMobile" placeholder="Mobile" minlength="10" maxlength="10" onkeydown="reserError('error_registerInputMobile')">
                                <label class="custom-error" id="error_registerInputMobile"></label>
                            </div>
                            <div class="form-group">
                                <select class="form-control custom-select" name="gender" id="registerGender" onchange="reserError('error_registerGender')">
                                    <option value="">Select</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                                <label class="custom-error" id="error_registerGender"></label>
                            </div>
<!--                            <div class="form-group">
                                <select class="form-control custom-select" name="userType" id="registerSelectType" onchange="reserError('error_registerSelectType')">
                                    <option value="">Select</option>
                                    <option value="user">User</option>
                                    <option value="loyer">Lawyer</option>
                                </select>
                                <label class="custom-error" id="error_registerSelectType"></label>
                            </div>-->
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input type="text" class="form-control form-control-user" name="dist" id="registerDist" placeholder="District" onkeydown="reserError('error_registerDist')">
                                    <label class="custom-error" id="error_registerDist"></label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control form-control-user" name="city" id="registerCity" placeholder="City" onkeydown="reserError('error_registerCity')">
                                    <label class="custom-error" id="error_registerCity"></label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input type="password" class="form-control form-control-user" name="password" id="registerInputPassword" placeholder="Password" minlength="6" maxlength="12" onkeydown="reserError('error_registerInputPassword')">
                                    <label class="custom-error" id="error_registerInputPassword"></label>
                                </div>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control form-control-user" name="confirmPassword" id="registerRepeatPassword" data-validation="confirmation" placeholder="Repeat Password" minlength="6" maxlength="12" onkeydown="reserError('error_registerRepeatPassword')">
                                    <label class="custom-error" id="error_registerRepeatPassword"></label>
                                </div>
                            </div>
                            <input type="submit" value="Register Account" class="btn btn-primary btn-user btn-block">

                        </form>
                        <hr>
                        <div class="text-center">
                            <a class="small" href="<?php echo base_url(); ?>lawyer/login">Already have an account? Login!</a>
                        </div>
                    </div>
                    <div id="toastCode"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('common/footer');
?>
<script src="<?php echo base_url(); ?>assets/js/custom/jquery.toaster.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>-->
<script src="<?php echo base_url(); ?>assets/js/custom/loyer.js"></script>
