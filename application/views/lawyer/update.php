<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('admin/layout/header');
$this->load->view('admin/layout/side_bar');
?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Lawyer</h1>
    <div class="alert alert-success" id="successMssg" style="display: none;">
    </div>
    <div class="alert alert-danger" id="errorMssg" style="display: none;">
    </div>
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
    <?php
    if (!empty($this->session->flashdata('msg_success'))) {
        ?>
        <div class="alert alert-success">

            <?php echo $this->session->flashdata('msg_success'); ?>
        </div>
        <?php
    }
    if ($this->session->flashdata('msg_error')) {
        ?>
        <div class="alert alert-danger">
            <?php echo $this->session->flashdata('msg_error'); ?>
        </div>
    <?php } ?>
    <hr/>
    <form class="user" id="userUpdate" method="POST" action="<?php echo base_url(); ?>lawyer/update" enctype="multipart/form-data">
        <div class="row">

            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>First Name:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <!--<span><?php echo!empty($loyer->firstName) ? $loyer->firstName : ''; ?></span>-->
                    <input type="text" class="form-control form-control-user" name="firstName" id="registerFirstName" placeholder="First Name" value="<?php echo!empty($loyer['firstName']) ? $loyer['firstName'] : ''; ?>">
                    <label class="custom-error" id="error_registerFirstName"></label>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>Last Name:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <span><?php // echo!empty($loyer->lastName) ? $loyer->lastName : '';            ?></span>
                    <input type="text" class="form-control form-control-user" name="lastName" id="registerLastName" placeholder="Last Name" value="<?php echo!empty($loyer['lastName']) ? $loyer['lastName'] : ''; ?>">
                    <label class="custom-error" id="error_registerlastName"></label>
                </div>

                <div class="clearer"></div>
            </div>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>Gender:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <span><?php // echo!empty($loyer->gender) ? ucfirst($loyer->gender) : '';         ?></span>
                    <?php
                    $gender = !empty($loyer['gender']) ? $loyer['gender'] : '';
                    $male = '';
                    $female = '';
                    if (!empty($gender)) {

                        if ($gender == 'male')
                            $male = 'selected';
                        else {
                            $female = 'selected';
                        }
                    }
                    ?>
                    <select class="form-control custom-select" name="gender" id="registerGender">
                        <option value="">Select</option>
                        <option value="male" <?php echo $male; ?>>Male</option>
                        <option value="female" <?php echo $female; ?>>Female</option>
                    </select>
                    <label class="custom-error" id="error_registerGender"></label>
                </div>

                <div class="clearer"></div>
            </div>

            <!--            <div class="col-lg-12">
                            <div class="col-sm-6 mg-3 mb-sm-0">
                                <span>Mobile:</span>
                            </div>
                            <div class="col-sm-6 mg-9 mb-sm-0">
                                <input type=text"" class="form-control form-control-user" name="mobile" id="registerInputMobile" placeholder="Mobile" value="<?php echo!empty($loyer['mobile']) ? $loyer['mobile'] : ''; ?>" pattern="[7-9]{1}[0-9]{9}">
                            </div>
                            <div class="clearer"></div>
                        </div>-->
            <hr/>
            <div class="clearfix"></div>
            <br/>
            <br/>
            <br/>

            <h1 class="h3 mb-4 custom-title">Address Details</h1>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>Dist:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <!--<span><?php echo!empty($loyer->dist) ? $loyer->dist : ''; ?></span>-->
                    <input type="text" class="form-control form-control-user" name="dist" id="registerDist" placeholder="District" required="true" value="<?php echo!empty($loyer['dist']) ? $loyer['dist'] : ''; ?>">
                    <label class="custom-error" id="error_registerDist"></label>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>Tal:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <span><?php // echo!empty(/$loyer->tal) ? $loyer->tal : '';            ?></span>
                    <input type="text" class="form-control form-control-user" name="tal" id="registerTal" placeholder="Tal" required="true" value="<?php echo!empty($loyer['tal']) ? $loyer['tal'] : ''; ?>">
                    <label class="custom-error" id="error_rregisterTal"></label>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>City:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <!--<span><?php echo!empty($loyer->city) ? $loyer->city : ''; ?></span>-->
                    <input type="text" class="form-control form-control-user" name="city" id="registerCity" placeholder="City" required="true" value="<?php echo!empty($loyer['city']) ? $loyer['city'] : ''; ?>">
                    <label class="custom-error" id="error_registerCity"></label>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>Pin:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <span><?php echo!empty($loyer->pin) ? $loyer->pin : ''; ?></span>
                    <input type="text" class="form-control form-control-user" name="pin" id="registerPin" placeholder="Pin" required="true" value="<?php echo!empty($loyer['pin']) ? $loyer['pin'] : ''; ?>">
                    <label class="custom-error" id="error_registerPin"></label>
                </div>
                <div class="clearer"></div>
            </div>
            <br/>
            <br/>
            <br/>
            <h1 class="h3 mb-4 custom-title">Educational Details</h1>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>Highest Degree:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="h_degree" id="h_degree" placeholder="Highest Education"  value="<?php echo!empty($loyer['h_degree']) ? $loyer['h_degree'] : ''; ?>">
                    <label class="custom-error" id="error_h_degree"></label>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>Specialization:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="specialization" id="specialization" placeholder="Any Specialization"  value="<?php echo!empty($loyer['specialization']) ? $loyer['specialization'] : ''; ?>">
                    <label class="custom-error" id="error_specialization"></label>
                </div>
                <div class="clearer"></div>
            </div>
            <br/>
            <br/>
            <br/>
            <h1 class="h3 mb-4 custom-title">About</h1>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>Profile picture:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <input type="file" name="profile" id="profile" class="form-control-file" id="exampleFormControlFile1">
                </div>
                <div class="clearer"></div>
            </div>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <span>Brief Your Self:</span>
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <textarea class="form-control" name="about" rows="5"><?php echo $loyer['about']; ?></textarea>
                    <label class="custom-error" id="error_about"></label>
                </div>
                <div class="clearer"></div>
            </div>
            <div class="col-lg-12">
                <div class="col-sm-6 mg-3 mb-sm-0">
                    <!--<span>Brief Your Self:</span>-->
                </div>
                <div class="col-sm-6 mg-9 mb-sm-0">
                    <input type="submit" name="submit" class="btn btn-primary" value="submit">
                </div>
                <div class="clearer"></div>
            </div>

        </div>
    </form>
</div>   

</div>

<?php
$this->load->view('admin/layout/footer');
?>
<script src="<?php echo base_url(); ?>assets/js/custom/jquery.toaster.js"></script>
<!--<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>-->
<script src="<?php echo base_url(); ?>assets/js/custom/loyer.js"></script>
