<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('common/header');
?>
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center" style="padding-top: 170px;">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <div class="alert alert-success" id="successMssg" style="display: none;">
                                    </div>
                                    <div class="alert alert-danger" id="errorMssg" style="display: none;">
                                    </div>
                                    <h3>Invalid Request.</h3>
                                    <p>This page was expired. Sorry for inconvenience.</p>
                                </div>


                                <hr>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<?php
$this->load->view('common/footer');
?>
<script>

    $(function () {
        function maxlength(value, maxlength) {
            return ((value !== undefined) && value !== null) ? value.length <= maxlength : true;
        }
        function minlength(value, minlength) {
            return ((value !== undefined) && value !== null) ? value.length >= minlength : true;
        }

        $("#user_otp").on('submit', function (e) {

            e.preventDefault();
            $('.custom-error').hide();
            var otpForm = $(this);
            var data = otpForm.serialize();

            if ($('#otpInput').val() === undefined || $('#otpInput').val() == '') {
                $('#error_otpInput').show().text('Please enter OTP.');
                return false;
            } else {
                if (!minlength($('#otpInput').val(), 6)) {
                    $('#error_otpInput').show().text('Please enter valid OTP.');
                    return false;
                }
            }

            $.ajax({
                url: otpForm.attr('action'),
                type: 'post',
                dataType: 'json',
                data: data,
                success: function (response) {

                    if (response.status && response.status == 1) {
                        var url = $('#base_url').val() + 'lawyer/success/';
                        $(location).attr('href', url);
                        setTimeout(function () {
                            $('#successMssg').hide();
                        }, 5000);
                        return true;
                    } else {
                        if (response.message) {
                            $('#errorMssg').show();
                            $('#errorMssg').text(response.message);
                            return false;
                        }
                        maketoast('Something went wrong!!', 'Error', 'danger');
                        return false;
                    }

                }
            });
        });

    });
    function resend() {
        var requestId = $('#requestId').val();
        var url = '<?php echo base_url() ?>lawyer/resendOtp/' + requestId;

        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {requestId: requestId},
            success: function (response) {
                if (response.status && response.status == 1) {
                    $('#successMssg').show();
                    $('#successMssg').text(response.message);
                    setTimeout(function () {
                        $('#successMssg').hide();
                        location.reload(true);
                    }, 5000);

                } else {
                    if (response.message) {
                        $('#errorMssg').show();
                        $('#errorMssg').text(response.message);
                        setTimeout(function () {
                            $('#errorMssg').hide();
                        }, 5000);
                        return false;
                    }
                    alert('Something went wrong!!');
                    return false;
                }

            }
        });
    }
</script>