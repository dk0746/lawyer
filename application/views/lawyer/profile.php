<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('admin/layout/header');
$this->load->view('admin/layout/side_bar');
?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <div class="col-sm-6 mg-9 mb-sm-0">
                <h1 class="h3 mb-4 text-gray-800">Lawyer</h1>
            </div>
            <div class="col-sm-6 mg-3 mb-sm-0" style="float:right">
                <h1 class="h3 mb-4 text-gray-800">
                    <a href="<?php echo base_url();?>lawyer/update" class="btn btn-primary btn-sm">Update</a>
                </h1>
            </div>
            <div class="clearer"></div>
        </div>
    </div>

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>First Name:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo!empty($loyer->firstName) ? $loyer->firstName : ''; ?></span>
            </div>
            <div class="clearer"></div>
        </div>
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>Last Name:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo!empty($loyer->lastName) ? $loyer->lastName : ''; ?></span>
            </div>

            <div class="clearer"></div>
        </div>
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>Gender:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo!empty($loyer->gender) ? ucfirst($loyer->gender) : ''; ?>sa</span>
            </div>

            <div class="clearer"></div>
        </div>
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>Email:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo!empty($loyer->email) ? $loyer->email : ''; ?></span>
            </div>

            <div class="clearer"></div>
        </div>
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>Mobile:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo!empty($loyer->mobile) ? $loyer->mobile : ''; ?></span>
            </div>
            <div class="clearer"></div>
        </div>
        <hr/>
        <div class="clearfix"></div>
        <br/>
        <br/>
        <br/>

        <h1 class="h3 mb-4 custom-title">Address Details</h1>
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>Dist:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo!empty($loyer->dist) ? $loyer->dist : ''; ?></span>
            </div>
            <div class="clearer"></div>
        </div>
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>Tal:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo!empty($loyer->tal) ? $loyer->tal : ''; ?></span>
            </div>
            <div class="clearer"></div>
        </div>
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>City:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo!empty($loyer->city) ? $loyer->city : ''; ?></span>
            </div>
            <div class="clearer"></div>
        </div>
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>Pin:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo!empty($loyer->pin) ? $loyer->pin : ''; ?></span>
            </div>
            <div class="clearer"></div>
        </div>
        <br/>
        <br/>
        <br/>
        <h1 class="h3 mb-4 custom-title">Educational Details</h1>
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>Highest Education:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo $loyer->h_degree;  ?></span>
            </div>
            <div class="clearer"></div>
        </div>
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>Specialization:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo $loyer->specialization;  ?></span>
            </div>
            <div class="clearer"></div>
        </div><br/>
        <br/>
        <br/>
        <h1 class="h3 mb-4 custom-title">About</h1>
        <div class="col-lg-12">
            <div class="col-sm-6 mg-3 mb-sm-0">
                <span>Brief Your self:</span>
            </div>
            <div class="col-sm-6 mg-9 mb-sm-0">
                <span><?php echo $loyer->about;  ?></span>
            </div>
            <div class="clearer"></div>
        </div>

    </div>
</div>   

</div>

<?php
$this->load->view('admin/layout/footer');
?>