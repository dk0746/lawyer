<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('common/header');
?>
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <?php
                                    if (!empty($this->session->flashdata('msg_success'))) {
                                        ?>
                                        <div class="alert alert-success">

                                            <?php echo $this->session->flashdata('msg_success'); ?>
                                        </div>
                                        <?php
                                    }
                                    if ($this->session->flashdata('msg_error')) {
                                        ?>
                                        <div class="alert alert-danger">
                                            <?php echo $this->session->flashdata('msg_error'); ?>
                                        </div>
                                    <?php } ?>
                                    <h1 class="h4 text-gray-900 mb-2">Forgot Your Password?</h1>
                                    <p class="mb-4">We get it, stuff happens. Just enter your email address below and we'll send you a link to reset your password!</p>
                                </div>
                                <form class="user" method="POST" action="<?php echo base_url(); ?>lawyer/forgot">
                                    <div class="form-group">
                                        <input type="email" name='email' class="form-control form-control-user" id="email" aria-describedby="emailHelp" placeholder="Enter Email Address...">
                                    </div>
                                    <input type="submit" value="Reset Password" class="btn btn-primary btn-user btn-block">
                                </form>
                                <hr>
                                <div class="text-center">
                                    <a class="small" href="<?php echo base_url(); ?>lawyer/login">Login!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<?php
$this->load->view('common/footer');
?>