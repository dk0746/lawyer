<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('common/header');
?>
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        
                        <div class="col-lg-12">
                            <div class="p-5">
                                <div class="text-center">
                                    <?php
                                    if (!empty($this->session->flashdata('msg_success'))) {
                                        ?>
                                        <div class="alert alert-success">

                                            <?php echo $this->session->flashdata('msg_success'); ?>
                                        </div>
                                        <?php
                                    }
                                    if ($this->session->flashdata('msg_error')) {
                                        ?>
                                        <div class="alert alert-danger">
                                            <?php echo $this->session->flashdata('msg_error'); ?>
                                        </div>
                                    <?php } ?>

                                    <p class="mb-4">Your verification process completed successfully. Please used below link to login.</p>
                                </div>

                                <hr>
                                <div class="text-center">
                                    <a class="small" href="<?php echo base_url(); ?>admin/login">Login!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<?php
$this->load->view('common/footer');
?>