<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('checkLogin')) {


    function checkLogin() {
        $CI = & get_instance();

        $CI->load->library('session');

        // This only returns the id does not set it.
        if ($CI->session->userdata('userid') && $CI->session->userdata('is_admin') == 0)
            return true;
        return false;
    }

}

if (!function_exists('isAdmin')) {


    function isAdmin() {
        $CI = & get_instance();

        $CI->load->library('session');

        // This only returns the id does not set it.
        if ($CI->session->userdata('role') && $CI->session->userdata('role') == 'admin')
            return true;
        return false;
    }

}
if (!function_exists('isLoyer')) {


    function isLoyer() {
        $CI = & get_instance();

        $CI->load->library('session');

        // This only returns the id does not set it.
        if ($CI->session->userdata('role') && $CI->session->userdata('role') == 'loyer')
            return true;
        return false;
    }

}

if (!function_exists('sendMail')) {


    function sendMail($to_email, $subject, $content) {

        $headers = 'From: Dilip Kale dilipkale746@gmail.com' . "\r\n";
        $headers .= 'Reply-To: ' . $to_email . "\r\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

        $result = mail($to_email, $subject, $content, $headers);
        if ($result) {
            return true;
        }

        return false;
    }

}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString . time();
}
