<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

    private $userType = 'admin';

    function __construct() {
        parent::__construct();
        $this->load->model('login_model', 'login');
        $this->load->helper('user_helper', 'user');
    }

    public function login() {
        $data['title'] = 'login';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|md5');

            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('msg_error', validation_errors());
                $this->load->view('admin/login', $data);
            } else {

                $postData = $this->input->post();
                if (empty($postData['email']) || empty($postData['password'])) {
                    $this->session->set_flashdata('msg_error', 'Please enter valid credential');
                    $this->load->view('admin/login', $data);
                } else {
                    $postData['userType'] = $this->userType;
                    if ($this->login->validate($postData)) {
                        redirect('admin/home');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Invalid credentials');
                        $this->load->view('admin/login',$data);
                    }
                }
            }
        } else {
            $this->load->view('admin/login', $data);
        }
    }

    public function changePassword() {
        if (!isAdmin()) {
            redirect('404');
        }
        $response = ['status' => 0, 'message' => ''];
        $data['title'] = 'Change Password';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('oldPassword', 'Old Password', 'trim|required|md5');
            $this->form_validation->set_rules('password', 'New Password', 'trim|required|md5');
            $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[password]|md5');

            if ($this->form_validation->run() === FALSE) {
                $response['message'] = validation_errors();
            } else {

                $postData = $this->input->post();

                $postData['userType'] = $this->userType;
                $loggedInId = $this->session->userdata('userid');

                $userData = $this->login->getLoggedInUser();
                if (!empty($userData)) {
                    if ($userData['password'] !== $postData['oldPassword']) {
                        $response['message'] = 'Please enter valid old password';
                    } else if ($this->login->updatePassword($postData['password'])) {
                        $response['message'] = 'Password updated successfully.';
                        $response['status'] = 1;
                    } else {
                        $response['message'] = 'Something went wrong.';
                    }
                } else {
                    $response['message'] = 'Something went wrong.';
                }
            }
            echo json_encode($response);
        } else {
            $this->load->view('admin/changePassword', $data);
        }
    }

    public function forgotPassword() {
        $data['title'] = 'Forgot Password';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('msg_error', validation_errors());
                $this->load->view('admin/forgot', $data);
                return;
            }
            $condtional = [
                'email' => $this->input->post('email'),
                'userType' => 'admin',
                'enabled' => '1'
            ];

            $userData = $this->login->getUser($condtional);

            if (!empty($userData)) {
                $restToken = $this->generateRandomString();
                $resetData = [
                    'user_id' => $userData['id'],
                    'rest_token' => $restToken
                ];

                $this->login->insert($resetData);
                $subject = 'Forgot password';
                $content = '';
                $content .= '<p>Hi ' . $userData['firstName'] . ' ' . $userData['lastName'] . '</p>';
                $content .= '<p>As per your request we have send password rest limk. Please click on below link to reset password.</p>';
                $content .= '<p><a href="http://www.ordrordr.com/reset/' . $restToken . '">Rest password</a></p>';

                if (sendMail($this->input->post('email'), $subject, $content)) {
                    $this->session->set_flashdata('msg_success', 'Email sent successfully.');
                    $this->load->view('admin/forgot', $data);
                } else {
                    $this->session->set_flashdata('msg_error', 'Email not sent successfully.');
                    $this->load->view('admin/forgot', $data);
                }
            }
        } else {
            $this->load->view('admin/forgot', $data);
        }
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString . time();
    }

    public function restPassword() {
        $data['title'] = 'Reset password';
        $token = $this->uri->segment(2);
        if (empty($token)) {
            redirect('404');
        }

        $condtional = [
            'rest_token' => $token,
            'is_active' => '1'
        ];

        $userData = $this->login->getRestRequest($condtional);

        if (!empty($userData)) {
            $password = $this->generateRandomString();
            $this->login->updatePassword($password, $userData['user_id']);
            $subject = 'Reset password';
            $content = '';
            $content .= '<p>Hi,</p>';
            $content .= '<p>Your new temporary password is: ' . $password . '</p>';
            $content .= '<p>Thank You.</p>';

            if (sendMail($userData['email'], $subject, $content)) {
//                $this->session->set_flashdata('msg_success', 'Email sent successfully.');
                $data['content'] = 'Your password has been reset successfully. We have sent new password on your registered mail id.';
                $this->load->view('reset', $data);
            } else {
//                $this->session->set_flashdata('msg_error', 'Email not sent successfully.');
                $data['content'] = 'Getting error to send email. Please contact administrator!!';
                $this->load->view('reset', $data);
            }
        } else {
            redirect('404');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('admin/login');
    }

    public function verify($param) {
        $token = $this->uri->segment(2);
        if (empty($token)) {
            redirect('404');
        }

        $condtional = [
            'token' => $token,
            'is_active' => '1'
        ];

        $userData = $this->login->getVerificationRequest($condtional);

        if (!empty($userData)) {
            $data['title'] = 'Verification';
            $this->login->update('user_verification', ['user_id' => $userData['user_id']], ['is_active' => 0]);
            $this->login->update('user', ['id' => $userData['user_id']], ['is_verify' => 1]);
            $this->load->view('verify', $data);
        } else {
            redirect('404');
        }
    }

}
