<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    private $userType = 'admin';
    private $status = '';

    function __construct() {
        parent::__construct();
        $this->load->model('login_model', 'login');
        $this->load->model('common_model', 'common');
        $this->load->helper('user_helper', 'user');
//        $this->load->li   brary('email');

        if (!isAdmin()) {
            $this->session->set_flashdata('msg_error', 'Please loggin first..');
            redirect('404');
        }
    }

    public function index() {

        $data = $this->common->getDashboardData();
        $data['title'] = 'Dashboard';
        $this->load->view('admin/home', $data);
    }

    public function listLawyer() {
        $data['title'] = 'Active Lawyer';
        $this->load->view('admin/list', $data);
    }

    public function block() {
        $data['title'] = 'Active Lawyers';
        $this->load->view('admin/list', $data);
    }

    public function pendingLawyers() {

        $data['title'] = 'Pending Lawyers';
        $this->load->view('admin/pending', $data);
    }

    public function blockLawyers() {

        $data['title'] = 'Blocked Lawyers';
        $this->load->view('admin/block', $data);
    }

    public function getActiveLawyers() {

        // POST data
        $postData = $this->input->post();
        $data = $this->common->getLawyers($postData, '1');


        echo json_encode($data);
    }

    public function getPendingLawyers() {

        // POST data
        $postData = $this->input->post();
        $data = $this->common->getPendingLawyers($postData, '0');


        echo json_encode($data);
    }

    public function getBlockLawyers() {

        // POST data
        $postData = $this->input->post();
        $data = $this->common->getBlockLawyers($postData, '2');


        echo json_encode($data);
    }

    public function getUsers() {
        $data['title'] = 'Users';
        $this->load->view('admin/user', $data);
    }

    public function viewLawyer($id) {

        $id = $this->uri->segment(3);
        if (empty($id)) {
            redirect('404');
        }
        $loyer = $this->common->getLawyer($id);
        $data['loyer'] = !empty($loyer) ? $loyer[0] : [];
        $data['title'] = 'Lawyer';
        $this->load->view('admin/view', $data);
    }

    public function process() {
        $response = ['status' => 0, 'message' => ''];
        $postData = $this->input->post();
        $loyers = $this->common->getLawyer($postData['loyer'], 1);
        $email = $loyers[0]['email'];

        if ($this->common->update($postData)) {
            $response['status'] = 1;
            $verificationToken = generateRandomString();
            $resetData = [
                'user_id' => $postData['loyer'],
                'token' => $verificationToken
            ];
            $subject = 'Approve registration request.';
            $content = '';
            $content .= '<p>Hi </p>';

            if (!empty($postData['status']) && $postData['status'] == 'unblock') {
                $content .= '<p>We have approved your request. You can login now.</p>';
            } else {
                $content .= '<p>We have approved your request. Please click below link to complete verification.</p>';
                $content .= '<p><a href="http://www.ordrordr.com/verify/' . $verificationToken . '">Rest password</a></p>';



                $this->common->addVerificationRequest($resetData);
            }

            $content .= '<p>Thank You</p>';

            if (sendMail($email, $subject, $content)) {
                $this->session->set_flashdata('msg_success', 'Email sent successfully.');
            } else {
                $this->session->set_flashdata('msg_error', 'Email not sent successfully.');
            }
        }
        echo json_encode($response);
    }

}
