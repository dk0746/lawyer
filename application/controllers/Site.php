<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

    private $userType = 'admin';

    function __construct() {
        parent::__construct();
        $this->load->model('login_model', 'login');
    }

    public function login() {
        $data['title'] = 'login';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|md5');

            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('msg_error', validation_errors());
                $this->load->view('site/login', $data);
            } else {

                $postData = $this->input->post();
                if (empty($postData['email']) || empty($postData['password'])) {
                    $this->session->set_flashdata('msg_error', 'Please enter valid credential');
                    $this->load->view('site/login', $data);
                } else {
                    $postData['userType'] = $this->userType;
                    if ($this->login->validate($postData)) {
                        redirect('admin/home');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Invalid credentials');
                        $this->load->view('site/login');
                    }
                }
            }
        } else {
            $this->load->view('site/login', $data);
        }
    }

}
