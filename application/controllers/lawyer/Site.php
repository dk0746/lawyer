<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

    private $userType = 'loyer';

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/loyer
     * 	- or -
     * 		http://example.com/index.php/loyer/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('login_model', 'login');
        $this->load->helper('user_helper', 'user');
    }

    public function login() {
        $data['title'] = 'login';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|md5');

            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('msg_error', validation_errors());
                $this->load->view('lawyer/login', $data);
            } else {

                $postData = $this->input->post();
                if (empty($postData['email']) || empty($postData['password'])) {
                    $this->session->set_flashdata('msg_error', 'Please enter valid credential');
                    $this->load->view('lawyer/login', $data);
                } else {
                    $postData['userType'] = $this->userType;
                    if ($this->login->validate($postData)) {
                        redirect('lawyer/home');
                    } else {
                        $this->session->set_flashdata('msg_error', 'Invalid credentials');
                        $this->load->view('lawyer/login', $data);
                    }
                }
            }
        } else {
            $this->load->view('lawyer/login', $data);
        }
    }

    public function add() {

        $response = [
            'status' => 0,
            'message' => '',
            'data' => Array()
        ];

        $this->form_validation->set_rules('firstName', 'Name', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('lastName', 'Name', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('mobile', 'Mobile No', 'trim|required|min_length[10]|max_length[11]');
        $this->form_validation->set_rules('gender', 'gender', 'trim|required');
        $this->form_validation->set_rules('userType', 'User Type', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[user.email]');
        $this->form_validation->set_rules('dist', 'District', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('city', 'City', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|md5');

        if ($this->form_validation->run() === FALSE) {
            $response['message'] = validation_errors();
        } else {

            if ($this->user->add()) {
                $response['status'] = 1;
                $response['message'] = 'Registration Successful!. You will notify by mail Once admin approved your request.';
            } else {
                $response['message'] = 'Error! Please try again later.';
            }
        }

        echo json_encode($response);
    }

    public function forgotPassword() {
        $data['title'] = 'Forgot Password';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

            if ($this->form_validation->run() === FALSE) {
                $this->session->set_flashdata('msg_error', validation_errors());
                $this->load->view('lawyer/forgot', $data);
                return;
            }
            $condtional = [
                'email' => $this->input->post('email'),
                'userType' => 'lawyer',
                'enabled' => '1'
            ];

            $userData = $this->login->getUser($condtional);

            if (!empty($userData)) {
                $restToken = $this->generateRandomString();
                $resetData = [
                    'user_id' => $userData['id'],
                    'rest_token' => $restToken
                ];

                $this->login->insert($resetData);
                $subject = 'Forgot password';
                $content = '';
                $content .= '<p>Hi ' . $userData['firstName'] . ' ' . $userData['lastName'] . '</p>';
                $content .= '<p>As per your request we have send password rest limk. Please click on below link to reset password.</p>';
                $content .= '<p><a href="http://www.ordrordr.com/reset/' . $restToken . '">Rest password</a></p>';

                if (sendMail($this->input->post('email'), $subject, $content)) {
                    $this->session->set_flashdata('msg_success', 'Email sent successfully.');
                    $this->load->view('lawyer/forgot', $data);
                } else {
                    $this->session->set_flashdata('msg_error', 'Email not sent successfully.');
                    $this->load->view('lawyer/forgot', $data);
                }
            } else {
                $this->session->set_flashdata('msg_error', 'Invalid mail id');
                $this->load->view('lawyer/forgot', $data);
            }
        } else {
            $this->load->view('lawyer/forgot', $data);
        }
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString . time();
    }

    public function restPassword() {
        $token = $this->uri->segment(2);
        if (empty($token)) {
            redirect('404');
        }

        $condtional = [
            'rest_token' => $token,
            'is_active' => '1'
        ];

        $userData = $this->login->getRestRequest($condtional);

        if (!empty($userData)) {
            $password = $this->generateRandomString();
            $this->login->updatePassword($password, $userData['user_id']);
            $subject = 'Reset password';
            $content = '';
            $content .= '<p>Hi,</p>';
            $content .= '<p>Your new temporary password is: ' . $password . '</p>';
            $content .= '<p>Thank You.</p>';

            if (sendMail($userData['email'], $subject, $content)) {
                $this->session->set_flashdata('msg_success', 'Email sent successfully.');
                $this->load->view('reset', $data);
            } else {
                $this->session->set_flashdata('msg_error', 'Email not sent successfully.');
                $this->load->view('rest', $data);
            }
        }
    }

    public function changePassword() {
        if (!isLoyer()) {
            redirect('404');
        }

        $data['title'] = 'Change Password';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->form_validation->set_rules('oldPassword', 'Old Password', 'trim|required|md5');
            $this->form_validation->set_rules('password', 'New Password', 'trim|required|md5');
            $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[password]|md5');

            if ($this->form_validation->run() === FALSE) {
                $response['message'] = validation_errors();
            } else {

                $postData = $this->input->post();

                $postData['userType'] = $this->userType;
                $loggedInId = $this->session->userdata('userid');

                $userData = $this->login->getLoggedInUser();
                if (!empty($userData)) {
                    if ($userData['password'] !== $postData['oldPassword']) {
                        $response['message'] = 'Please enter valid old password';
                    } else if ($this->login->updatePassword($postData['password'])) {
                        $response['message'] = 'Password updated successfully.';
                        $response['status'] = 1;
                    } else {
                        $response['message'] = 'Something went wrong.';
                    }
                } else {
                    $response['message'] = 'Something went wrong.';
                }
            }
            echo json_encode($response);
        } else {
            $this->load->view('lawyer/changePassword', $data);
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('lawyer/login');
    }

}
