<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Lawyer extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/loyer
     * 	- or -
     * 		http://example.com/index.php/loyer/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('user_model', 'user');
    }

    public function index() {
        $data['title'] = 'Register';
        $this->load->view('lawyer/index', $data);
    }

    public function add() {

        $response = [
            'status' => 0,
            'message' => '',
            'data' => Array()
        ];

        $this->form_validation->set_rules('firstName', 'Name', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('lastName', 'Name', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('mobile', 'Mobile No', 'trim|required|min_length[10]|max_length[11]|is_unique[user.mobile]');
        $this->form_validation->set_rules('gender', 'gender', 'trim|required');
//        $this->form_validation->set_rules('userType', 'User Type', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[user.email]');
        $this->form_validation->set_rules('dist', 'District', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('city', 'City', 'trim|required|min_length[3]|max_length[50]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|md5');

        if ($this->form_validation->run() === FALSE) {
            $response['message'] = validation_errors();
        } else {
            $postData = $this->input->post();
            $otp = $this->random_strings(6);
            $OtpResponse = $this->sendOtp($otp, $postData['mobile']);
            $otpResponseData = json_decode($OtpResponse, 1);
            if (empty($otpResponseData['status']) || $otpResponseData['status'] != 'success') {
                $response['message'] = 'Failed to send OTP';
            } else {
                $postData['userType'] = 'loyer';
                $requestData = [
                    'request' => json_encode($postData),
                    'otp' => $otp
                ];

//            $myfile = file_put_contents('test.txt', $otpResponseData['status'] . PHP_EOL, FILE_APPEND | LOCK_EX);
                $requestId = $this->user->addRequest($requestData);
                $encodedId = base64_encode($requestId . '_' . time());
                $response['data']['requestId'] = $encodedId;

                $response['status'] = 1;
                $response['message'] = 'OTP send Successfully.';
            }
        }
        echo json_encode($response);
    }

    public function sendOtp($otp, $mobile) {
        $username = "kaledilip46@gmail.com";
        $hash = "d6fd5fb8d88025672409007d4cae3a5746193f77ada1bb2065b76f854cfe382e";

        // Config variables. Consult http://api.textlocal.in/docs for more info.
        $test = "0";

        // Data for text message. This is the text message data.
        $sender = "TXTLCL"; // This is who the message appears to be from.
        $numbers = "91" . $mobile; // A single number or a comma-seperated list of numbers
        $message = "The scret code to your success is " . $otp . ' Have a wonderful day ahead.';
        // 612 chars or less
        // A single number or a comma-seperated list of numbers
        $message = urlencode($message);
        $data = "username=" . $username . "&hash=" . $hash . "&message=" . $message . "&sender=" . $sender . "&numbers=" . $numbers . "&test=" . $test;
        $ch = curl_init('http://api.textlocal.in/send/?');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch); // This is the result from the API
        curl_close($ch);
//        $txt = "user id date";
//        $myfile = file_put_contents('test.txt', $response . PHP_EOL, FILE_APPEND | LOCK_EX);

        return $response;
    }

    public function verifyOtp() {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $postData = $this->input->post();
            $requestId = $this->getDecodedRequestId($postData['requestId']);
            $requestData = $this->user->getRequest($requestId);

            if (!empty($requestData)) {

                $request = json_decode($requestData[0]['request'], 1);

                if ($postData['otp'] === $requestData[0]['otp'] && $requestData[0]['expired'] == 0) {
                    $this->user->update('user_otp', ['valid' => 0], ['id' => $requestId]);
                    $this->user->add($request);
                    $response['status'] = 1;
                } else {
                    $response['message'] = "Invalid OTP.";
                }
            } else {
                $response['message'] = "Request not found.";
            }
            echo json_encode($response);
        } else {

            $data['title'] = 'Verify OTP';
            $data['requestId'] = $this->uri->segment(3);
            if (empty($data['requestId'])) {
                redirect('404');
            }

            $requestId = $this->getDecodedRequestId($data['requestId']);
            $requestData = $this->user->getRequest($requestId);

            if (empty($requestData) || $requestData[0]['attempt'] >= 3) {
                redirect('invalid');
            }
            $this->load->view('lawyer/otp', $data);
        }
    }

    public function expired() {
        $response = ['status' => 0, 'message' => ''];

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $postData = $this->input->post();
            $requestId = $this->getDecodedRequestId($postData['requestId']);
            $requestData = $this->user->getRequest($requestId);

            if (!empty($requestData)) {
//                $this->user->update('user_otp', ['expired' => 1], ['id' => $requestId]);
                $response['message'] = "OTP was expired.Please resend.";
                $response['status'] = 1;
            } else {
                $response['message'] = "Request not found.";
            }
            echo json_encode($response);
        }
    }

    public function getDecodedRequestId($requestId) {
        if (!empty($requestId)) {
            $decodeData = base64_decode($requestId);
            $requestData = explode('_', $decodeData);
            return $requestData[0];
        }
        return 0;
    }

    public function resendOtp() {

        $response = ['status' => 0, 'message' => '', 'data' => ''];
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $postData = $this->input->post();
            $otp = $this->random_strings(6);
            $requestId = $this->getDecodedRequestId($postData['requestId']);

            $requestData = $this->user->getRequest($requestId);
            if (!empty($requestData)) {
                if ($requestData[0]['attempt'] >= 3) {
                    $response['status'] = 3;
                    $response['message'] = 'Attempt exceed.';
                } else {
                    $request = json_decode($requestData[0]['request'], 1);
                    $attempt = $requestData[0]['attempt'] + 1;
                    $mobile = $request['mobile'];
                    $OtpResponse = $this->sendOtp($otp, $mobile);
                    $OtpResponseData = json_decode($OtpResponse, 1);
                    if ($OtpResponseData['status'] == 'success') {
                        $response['data'] = 'done';
                    }

                    if ($this->user->updateRequest($otp, $requestId, $attempt)) {
                        $response['status'] = 1;
                        $response['message'] = 'OTP send successfully.';
                    }
                }
            } else {
                $response['message'] = 'Invalid request.';
            }
            echo json_encode($response);
        }
    }

    function random_strings($length_of_string) {

        // String of all alphanumeric character 
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

        // Shufle the $str_result and returns substring 
        // of specified length 
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

    public function success() {

        $data['title'] = 'Thank you';
        $this->load->view('lawyer/success', $data);
    }

}
