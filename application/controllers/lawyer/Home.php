<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    private $userType = 'loyer';
    private $lawyerId = '';
    private $status = '';

    function __construct() {
        parent::__construct();
        $this->load->model('login_model', 'login');
        $this->load->model('common_model', 'common');
        $this->load->model('appointment_model', 'app');
        $this->load->helper('user_helper', 'user');
//        $this->load->li   brary('email');

        if (!isLoyer()) {
            $this->session->set_flashdata('msg_error', 'Please loggin first..');
            redirect('404');
        }
        $this->lawyerId = $this->session->userdata('userid');
    }

    public function index() {

        $data = $this->app->getDashboardData($this->lawyerId);
        $data['title'] = 'Dashboard';
        $this->load->view('lawyer/home', $data);
    }

    public function profile() {

        $lawyer = $this->common->getLawyer($this->lawyerId);
        $data['loyer'] = !empty($lawyer) ? $lawyer[0] : [];
        $data['title'] = 'Lawyer';
        $this->load->view('lawyer/profile', $data);
    }

    public function update() {
        $id = $this->session->userdata('userid');

        $lawyer = $this->common->getLawyer($id, 1);
        $data['loyer'] = !empty($lawyer) ? $lawyer[0] : [];
        $data['title'] = 'Lawyer';
        ini_set('max_input_vars','2000' );
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $response = ['status' => 0, 'message' => ''];
            $data['loyer'] = $this->input->post();
            $this->form_validation->set_rules('firstName', 'Name', 'trim|required|min_length[3]|max_length[50]');
            $this->form_validation->set_rules('lastName', 'Name', 'trim|required|min_length[3]|max_length[50]');
//            $this->form_validation->set_rules('mobile', 'Mobile No', 'trim|required|min_length[10]|max_length[11]');
            $this->form_validation->set_rules('gender', 'gender', 'trim|required');
            $this->form_validation->set_rules('dist', 'District', 'trim|required|min_length[3]|max_length[50]');
            $this->form_validation->set_rules('Tal', 'Tal', 'trim|min_length[3]|max_length[50]');
            $this->form_validation->set_rules('city', 'City', 'trim|required|min_length[3]|max_length[50]');
            $this->form_validation->set_rules('pin', 'Pin', 'trim|min_length[3]|max_length[10]');
            $this->form_validation->set_rules('h_degree', 'Highest education', 'trim|min_length[3]|max_length[50]');
            $this->form_validation->set_rules('specialization', 'Specialization', 'trim|min_length[3]|max_length[50]');


            if ($this->form_validation->run() === FALSE) {
                $response['message'] = validation_errors();
            } else {
                $postData = $this->input->post();
                
                if (!empty($_FILES['profile']['name'])) {
                    $fileData = $this->store();
                    if (empty($fileData['image_metadata'])) {
                        $response['message'] = $fileData['error'];
                        echo json_encode($response);
                        return;
                    }
                    $postData['profile'] = $fileData['image_metadata']['file_name'];
                }
                if ($this->common->updateLawyer($id, $postData)) {
                    $response['message'] = 'Profile updated successfully.';
                    $response['status'] = 1;
                } else {
                    $response['message'] = 'Something went wrong.';
                }
            }
            echo json_encode($response);
        } else {
            $this->load->view('lawyer/update', $data);
        }
    }

    public function store() {

        $image = time() . '-' . $_FILES["profile"]['name'];
        $config['upload_path'] = './assets/user_img/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 2000;
        $config['max_width'] = 3000;
        $config['max_height'] = 3000;
        $config['file_name'] = $image;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('profile')) {
            return $error = array('error' => $this->upload->display_errors());
        } else {
            return array('image_metadata' => $this->upload->data());
        }
    }

    public function appointment() {
        $data['title'] = 'Appointment';
        $this->load->view('lawyer/appointment', $data);
    }
    public function app() {
        
        $postData = $this->input->post();
        $condtionalArray = [
            'lawyer_id' => $this->lawyerId
        ];
        $data = $this->app->getAppointment($postData, $condtionalArray);
        echo json_encode($data);
    }

    public function dailyApp() {

        $data['title'] = "Today's Appointment";
        $this->load->view('lawyer/daily', $data);
    }

    public function upcomingApp() {
        $data['title'] = 'Upcomming Appointment';
        $this->load->view('lawyer/upcomming', $data);
    }

    public function getDailyAppointment() {

        $date = new DateTime();
        $app_date = $date->format('Y-m-d');

        $postData = $this->input->post();
        $condtionalArray = [
            'lawyer_id' => $this->lawyerId,
            'date(app_date)' => $app_date
        ];
        $data = $this->app->getAppointment($postData, $condtionalArray);


        echo json_encode($data);
    }

    public function getUpcommingAppointment() {

        $date = new DateTime();
        $app_date = $date->format('Y-m-d');

        $postData = $this->input->post();
        $condtionalArray = [
            'lawyer_id' => $this->lawyerId,
            'date(app_date) >' => $app_date
        ];
        $data = $this->app->getAppointment($postData, $condtionalArray);


        echo json_encode($data);
    }

}
