<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'welcome/index';
$route['404_override'] = 'notFound';
$route['translate_uri_dashes'] = FALSE;

$route['register'] = 'lawyer/lawyer/index';
$route['add'] = 'lawyer/lawyer/add';

$route['lawyer/login'] = 'lawyer/site/login';
$route['lawyer/sendOtp'] = 'lawyer/lawyer/sendOtp';
$route['lawyer/verifyOtp(.+)'] = 'lawyer/lawyer/verifyOtp/$1';
$route['lawyer/resendOtp(.+)'] = 'lawyer/lawyer/resendOtp/$1';
$route['lawyer/expired'] = 'lawyer/lawyer/expired';
$route['lawyer/success'] = 'lawyer/lawyer/success';

$route['lawyer/changePassword'] = 'lawyer/site/changePassword';
$route['lawyer/forgot'] = 'lawyer/site/forgotPassword';
$route['lawyer/profile'] = 'lawyer/home/profile';
$route['lawyer/logout'] = 'lawyer/site/logout';

$route['lawyer/home'] = 'lawyer/home/index';
$route['lawyer/update'] = 'lawyer/home/update';
$route['lawyer/appointment'] = 'lawyer/home/appointment';
$route['lawyer/app'] = 'lawyer/home/app';
$route['lawyer/dailyApp'] = 'lawyer/home/dailyApp';
$route['lawyer/upcomingApp'] = 'lawyer/home/upcomingApp';
$route['lawyer/daily'] = 'lawyer/home/getDailyAppointment';
$route['lawyer/upcomming'] = 'lawyer/home/getUpcommingAppointment';

$route['admin/login'] = 'admin/site/login';
$route['admin/logout'] = 'admin/site/logout';
$route['admin/changePassword'] = 'admin/site/changePassword';
$route['admin/forgot'] = 'admin/site/forgotPassword';
$route['reset(.+)'] = 'admin/site/restPassword/$1';
$route['verify(.+)'] = 'admin/site/verify/$1';

$route['admin/home'] = 'admin/home/index';
$route['admin/lawyer'] = 'admin/home/listLawyer';
$route['admin/pendingLawyer'] = 'admin/home/pendingLawyers';
$route['admin/blockLawyer'] = 'admin/home/blockLawyers';
$route['admin/lawyer/process'] = 'admin/home/process/';
$route['admin/lawyer(.+)'] = 'admin/home/viewLawyer/$1';

$route['admin/active'] = 'admin/home/getActiveLawyers';
$route['admin/pendings'] = 'admin/home/getPendingLawyers';
$route['admin/block'] = 'admin/home/getBlockLawyers';
$route['admin/users'] = 'admin/home/getUsers';
$route['user/login'] = 'user/site/login';
$route['site/login'] = 'site/login';
$route['invalid'] = 'error/invalid';