<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common_model extends CI_Model {

    private $table;

    function __construct() {
        parent::__construct();
        $this->table = 'user';
    }

    function getLawyers($postData = null, $status = '') {

        $response = array();

        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search 
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (firstName like '%" . $searchValue . "%' or lastName like '%" . $searchValue . "%' or mobile like '%" . $searchValue . "%'  or email like '%" . $searchValue . "%' or city like'%" . $searchValue . "%' ) ";
        }


        ## Total number of records without filtering
        $this->db->select('count(*) as allcount');
        $records = $this->db->get($this->table)->result();
        $totalRecords = $records[0]->allcount;

        ## Total number of record with filtering
        $this->db->select('count(*) as allcount');
        $this->db->where(['userType' => 'loyer', 'enabled' => $status]);
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $records = $this->db->get($this->table)->result();
        $totalRecordwithFilter = $records[0]->allcount;

        ## Fetch records
        $this->db->select('id,firstName,lastName,mobile,email,dist,tal,pin,city');
        $this->db->where(['userType' => 'loyer', 'enabled' => $status]);

        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by($columnName, $columnSortOrder);
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get($this->table)->result();

        $data = array();

        foreach ($records as $record) {
            $action = '';
            if ($status == '0') {
                $action = '<a href="#" onClick="viewLoyer(' . $record->id . ');" class="btn btn-primary btn-sm">View</a>'
                        . '<a href="#" onClick="processLoyer(' . $record->id . ');" class="btn btn-primary btn-sm">Accept</a>';
            } else if ($status == '1') {
                $action = '<a href="#" onClick="viewLoyer(' . $record->id . ');" class="btn btn-primary btn-sm">View</a>
                 <a href="#" onClick="BlockLoyer(' . $record->id . ');" class="btn btn-danger btn-sm">Block</a>';
            } else if ($status == '2') {
                $action = '<a href="#" onClick="viewLoyer(' . $record->id . ');" class="btn btn-primary btn-sm">View</a>'
                        . '<a href="#" onClick="processLoyer(' . $record->id . ');" class="btn btn-primary btn-sm">Un Block</a>';
            }
            $data[] = array(
                $record->firstName,
                $record->lastName,
                $record->mobile,
                $record->email,
                $record->dist,
                $record->tal,
                $record->pin,
                $record->city,
                $action,
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        return $response;
    }

    public function getPendingLawyers($postData) {
        return $this->getLawyers($postData, '0');
    }

    public function getBlockLawyers($postData) {
        return $this->getLawyers($postData, '2');
    }

    public function getLawyer($id, $isArray = '') {

        $this->db->select('*');
        $this->db->where(['userType' => 'loyer', 'id' => $id]);
        if ($isArray != '') {
            $records = $this->db->get($this->table)->result_array();
        } else {
            $records = $this->db->get($this->table)->result();
        }

        if (!empty($records)) {
            return $records;
        }
        return false;
    }

    public function getDashboardData() {

        $data['activeLoyer'] = $this->getLawyerCountByCode('1');
        $data['pendingLoyer'] = $this->getLawyerCountByCode('0');
        $data['blockLoyer'] = $this->getLawyerCountByCode('2');
        return $data;
    }

    public function getLawyerCountByCode($code) {

        $this->db->select('count(*) as allcount');
        $this->db->where(['enabled' => $code, 'userType' => 'loyer']);
        $records = $this->db->get($this->table)->result();
        return $records[0]->allcount;
    }

    public function update($data) {
        $loyer = $this->security->xss_clean($data['loyer']);
        $status = $this->security->xss_clean($data['status']);

        if (!empty($loyer) && !empty($status)) {
            $statusCode = $this->getStatusCode($status);
            if (!empty($statusCode)) {
                $this->db->where('id', $loyer);
                $this->db->update('user', ['enabled' => $statusCode]);
                return true;
            }
        }
        return false;
    }

    public function getStatusCode($status) {
        $statusCode = '';
        switch ($status) {
            case 'active':
            case 'unblock':
                $statusCode = '1';
                break;
            case 'pending':$statusCode = '0';
                break;
            case 'block':$statusCode = '2';
                break;

            default:
                $statusCode = '';
                break;
        }
        return $statusCode;
    }

    public function updateLawyer($id, $data) {
        if (!empty($id) && !empty($data)) {
            $this->db->where('id', $id);
            $this->db->update('user', $data);
            return true;
        }
        return false;
    }

    public function addVerificationRequest($data) {

        if (!empty($data))
            return $this->db->insert('user_verification', $data);

        return false;
    }

}

?>