<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model {

    private $table;

    function __construct() {
        parent::__construct();
        $this->table = 'user';
    }

    public function add($postData) {

        $data = array(
            'firstName' => $postData['firstName'],
            'lastName' => $postData['lastName'],
            'email' => $postData['email'],
            'mobile' => $postData['mobile'],
            'gender' => $postData['gender'],
            'dist' => $postData['dist'],
            'city' => $postData['city'],
            'userType' => $postData['userType'],
            'password' => $postData['password']
        );
        return $this->db->insert($this->table, $data);
    }

    public function addRequest($data) {
        $date = new DateTime();
        $otp_date = $date->format('Y-m-d H:i:s');
        $data['otp_time'] = $otp_date;

        $this->db->insert('user_otp', $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function getRequest($requestId) {

        $this->db->select('*');
        $this->db->where(['id' => $requestId, 'valid' => '1']);
        return $this->db->get('user_otp')->result_array();
    }

    public function updateRequest($otp, $requestId, $attempt = 0) {
        $date = new DateTime();
        $otp_date = $date->format('Y-m-d H:i:s');

        $this->db->where('id', $requestId);
        $this->db->update('user_otp', ['otp' => $otp, 'otp_time' => $otp_date, 'expired' => '1', 'attempt' => $attempt]);
        return true;
    }

    public function update($table, $data, $condition) {
        $this->db->where($condition);
        $this->db->update($table, $data);
    }

}

?>