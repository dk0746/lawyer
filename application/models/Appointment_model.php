<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Appointment_model extends CI_Model {

    private $table;

    function __construct() {
        parent::__construct();
        $this->table = 'appointment';
    }

    function getAppointment($postData = null, $condtionalArray = []) {

        $response = array();

        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start'];
        $rowperpage = $postData['length']; // Rows display per page
        $columnIndex = $postData['order'][0]['column']; // Column index
        $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        ## Search 
        $searchQuery = "";
        if ($searchValue != '') {
            $searchQuery = " (firstName like '%" . $searchValue . "%' or lastName like '%" . $searchValue . "%' or mobile like '%" . $searchValue . "%'  or email like '%" . $searchValue . "%' or city like'%" . $searchValue . "%' ) ";
        }

        

        $this->db->select('count(*) as allcount');
        $this->db->from('appointment');
        $this->db->join('user', 'user.id = appointment.user_id', 'left');
        if (!empty($condtionalArray)) {
            $this->db->where($condtionalArray);
        }
        $query = $this->db->get();
        $records = $query->result_array();
        $totalRecords = $records[0]['allcount'];
        ## Total number of record with filtering

        $this->db->select('count(*) as allcount');
        $this->db->from('appointment');
        $this->db->join('user', 'user.id = appointment.user_id', 'left');
        
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        if (!empty($condtionalArray)) {
            $this->db->where($condtionalArray);
        }
        $records = $this->db->get();
        $records = $query->result_array();
        $totalRecordwithFilter = $records[0]['allcount'];

        ## Fetch records
        $this->db->select('user.id,user.firstName,user.lastName,user.mobile,user.email,user.dist,user.tal,user.pin,user.city');
        $this->db->from('appointment');
        $this->db->join('user as user', 'user.id = appointment.user_id', 'left');
        if (!empty($condtionalArray)) {
            $this->db->where($condtionalArray);
        }
        
        if ($searchQuery != '')
            $this->db->where($searchQuery);
        $this->db->order_by($columnName, $columnSortOrder);
        $this->db->limit($rowperpage, $start);
        $records = $this->db->get()->result();
//        echo $this->db->last_query();die;
        $data = array();

        foreach ($records as $record) {
            
            $data[] = array(
                $record->firstName,
                $record->lastName,
                $record->mobile,
                $record->email,
                $record->dist,
                $record->tal,
                $record->pin,
                $record->city
            );
        }

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );
//        echo '<pre>';
//        print_r($response);
//        die;
        return $response;
    }

    public function getPendingLawyers($postData) {
        return $this->getLawyers($postData, '0');
    }

    public function getBlockLawyers($postData) {
        return $this->getLawyers($postData, '2');
    }

    public function getLawyer($id, $isArray = '') {

        $this->db->select('*');
        $this->db->where(['userType' => 'loyer', 'id' => $id]);
        if ($isArray != '') {
            $records = $this->db->get($this->table)->result_array();
        } else {
            $records = $this->db->get($this->table)->result();
        }

        if (!empty($records)) {
            return $records;
        }
        return false;
    }

    public function getDashboardData($lawyerId) {

        $data['todayAppCount'] = $this->getDailyApp($lawyerId);
        $data['upcommingAppCount'] = $this->getUpcommingApp($lawyerId);
        return $data;
    }

    public function getDailyApp($lawyerId) {

        $date = new DateTime();
        $app_date = $date->format('Y-m-d');

        $condtionalArray = [
            'lawyer_id' => $lawyerId,
            'date(app_date)' => $app_date
        ];

        $dailyAppointment = $this->getApp($condtionalArray);
        if (!empty($dailyAppointment)) {
            return count($dailyAppointment);
        }
        return '0';
    }

    public function getUpcommingApp($lawyerId) {

        $date = new DateTime();
        $app_date = $date->format('Y-m-d');

        $condtionalArray = [
            'lawyer_id' => $lawyerId,
            'date(app_date) >' => $app_date
        ];

        $upcommingAppointment = $this->getApp($condtionalArray);
        if (!empty($upcommingAppointment)) {
            return count($upcommingAppointment);
        }
        return '0';
    }

    public function getApp($condtionalArray = []) {
        $this->db->select('user.*,appointment.app_date');
        $this->db->from('appointment');
        $this->db->join('user', 'user.id = appointment.user_id', 'left');
        if (!empty($condtionalArray)) {
            $this->db->where($condtionalArray);
        }
        $query = $this->db->get();
        return $result = $query->result_array();
    }

    public function update($data) {
        $loyer = $this->security->xss_clean($data['loyer']);
        $status = $this->security->xss_clean($data['status']);

        if (!empty($loyer) && !empty($status)) {
            $statusCode = $this->getStatusCode($status);
            if (!empty($statusCode)) {
                $this->db->where('id', $loyer);
                $this->db->update('user', ['enabled' => $statusCode]);
                return true;
            }
        }
        return false;
    }

    public function getStatusCode($status) {
        $statusCode = '';
        switch ($status) {
            case 'active':
            case 'unblock':
                $statusCode = '1';
                break;
            case 'pending':$statusCode = '0';
                break;
            case 'block':$statusCode = '2';
                break;

            default:
                $statusCode = '';
                break;
        }
        return $statusCode;
    }

    public function updateLawyer($id, $data) {
        if (!empty($id) && !empty($data)) {
            $this->db->where('id', $id);
            $this->db->update('user', $data);
            return true;
        }
        return false;
    }

    public function addVerificationRequest($data) {

        if (!empty($data))
            return $this->db->insert('user_verification', $data);

        return false;
    }

}

?>