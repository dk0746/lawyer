<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model {

    private $table;

    function __construct() {
        parent::__construct();
        $this->table = 'user';
        $this->load->library('session');
    }

    public function validate($data) {

        $username = $this->security->xss_clean($this->input->post('email'));
        $password = $this->security->xss_clean($this->input->post('password'));


        $data = array(
            'email' => $username,
            'password' => $password,
            'userType' => $data['userType'],
            'is_verify' => 1
        );
        if ($data['userType'] == 'admin') {
            unset($data['is_verify']);
        }

        // Prep the query
        $query = $this->db->get_where('user', $data);
        $result = $query->result_array();
        if (!empty($result) && count($result) == 1) {
            // If there is a user, then create session data
            $result = $result[0];
            $data = array(
                'userid' => $result['id'],
                'firstName' => $result['firstName'],
                'lastName' => $result['lastName'],
                'role' => $result['userType'],
                'profile' => $result['profile']
            );
            $this->session->set_userdata($data);
            return true;
        }
        // If the previous process did not validate
        // then return false
    }

    public function getLoggedInUser() {

        if ($this->session->userdata('userid')) {
            $query = $this->db->get_where('user', ['id' => $this->session->userdata('userid')]);
            $result = $query->result_array();
            if (!empty($result) && count($result) == 1) {
                return $result[0];
            }
        }
        return false;
    }

    public function updatePassword($password, $user_id = '') {
        $id = $this->session->userdata('userid');
        if (!empty($user_id)) {
            $id = $user_id;
        }

        if ($id) {
            $this->db->where('id', $id);
            $this->db->update('user', ['password' => $password]);
            return true;
        }
        return false;
    }

    public function getUser($condtionalArray) {

        if (!empty($condtionalArray)) {
            $query = $this->db->get_where('user', $condtionalArray);
            $result = $query->result_array();
            if (!empty($result) && count($result) == 1) {
                return $result[0];
            }
        }
        return false;
    }

    public function insert($data) {
        if (!empty($data))
            return $this->db->insert('user_forgot', $data);

        return false;
    }

    public function getRestRequest($condtionalArray) {
        if (!empty($condtionalArray)) {

            $this->db->select('user_forgot.*,user.email');
            $this->db->from('user_forgot');
            $this->db->join('user', 'user.id = user_forgot.user_id', 'left');
            $this->db->where($condtionalArray);

            $query = $this->db->get();
            $result = $query->result_array();

            if (!empty($result) && count($result) == 1) {
                return $result[0];
            }
        }
        return false;
    }

    public function getVerificationRequest($condtionalArray) {
        if (!empty($condtionalArray)) {

            $this->db->select('user_verification.*,user.email');
            $this->db->from('user_verification');
            $this->db->join('user', 'user.id = user_verification.user_id', 'left');
            $this->db->where($condtionalArray);

            $query = $this->db->get();
            $result = $query->result_array();

            if (!empty($result) && count($result) == 1) {
                return $result[0];
            }
        }
        return false;
    }

    public function update($table, $condition, $data) {
        if (!empty($table) && !empty($condition) && !empty($data)) {
            $this->db->where($condition);
            $this->db->update($table, $data);
            return true;
        }
        return false;
    }

}

?>